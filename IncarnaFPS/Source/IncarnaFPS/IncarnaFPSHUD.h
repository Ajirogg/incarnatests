// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "IncarnaFPSHUD.generated.h"

UCLASS()
class AIncarnaFPSHUD : public AHUD
{
	GENERATED_BODY()

public:
	AIncarnaFPSHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

