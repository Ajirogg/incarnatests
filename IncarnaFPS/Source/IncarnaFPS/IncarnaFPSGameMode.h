// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IncarnaFPSGameMode.generated.h"

UCLASS(minimalapi)
class AIncarnaFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AIncarnaFPSGameMode();
};



