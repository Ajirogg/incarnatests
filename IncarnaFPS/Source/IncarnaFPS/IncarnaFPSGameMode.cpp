// Copyright Epic Games, Inc. All Rights Reserved.

#include "IncarnaFPSGameMode.h"
#include "IncarnaFPSHUD.h"
#include "IncarnaFPSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AIncarnaFPSGameMode::AIncarnaFPSGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AIncarnaFPSHUD::StaticClass();
}
